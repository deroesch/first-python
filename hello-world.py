# A simple http-fetching test
import requests

# Test URL
url = 'http://example.com'


# define the Fetcher class
class MyFetcher:
    def fetch(self, url):
        r = requests.get(url)
        return r.text


# Use the class to fetch the URL
print(MyFetcher().fetch(url))
